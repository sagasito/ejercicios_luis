#!/bin/bash
ARCHIVO=""
VAR1="" # Variable para saber si existe el fichero de monitorizacion
correo =""

function fichero {
    
    ARCHIVO1=`date +%Y%m%d.txt`
    ARCHIVO2="Monitor_servidor_"
    ARCHIVO=$ARCHIVO2$ARCHIVO1  
    
    if
        [ ! -f $ARCHIVO ]; then # Mira si no existe el fichero Monitor_servidor_AAAAMMDD.txt en el directorio de ejecución.
          VAR1="1" # Pone la variable VAR1 a 1
    else
          VAR1="0"  # Pone la variable VAR1 a 0 si existe el fichero Monitor_servidor_AAAAMMDD.txt en el directorio de ejecución.
    fi
}

function numCpus { # Muestra el nombre del servidor y el  número de CPU's 
                   # existentes en la maquina que ejecuta el bash
    
    echo "====================="  >> $ARCHIVO
    echo "Nombre del Servidor  "  >> $ARCHIVO
    hostname >> $ARCHIVO
    echo ""
    echo "====================="  >> $ARCHIVO
    echo "Número de CPU's:     "  >> $ARCHIVO
    cat /proc/cpuinfo | grep processor | wc -l >> $ARCHIVO
    echo "====================="  >> $ARCHIVO
}

function dirIP { # Muestra la dirección IP
    echo "====================="  >> $ARCHIVO
    echo "    Dirección IP     "  >> $ARCHIVO
    echo "====================="  >> $ARCHIVO
    ifconfig | grep inet >> $ARCHIVO
    echo "====================="  >> $ARCHIVO
}

function PuertosUSBPCI { # Muestra información de los puertos USB y PCI existentes en la maquina que ejecuta el bash
    echo "====================="  >> $ARCHIVO
    echo "                     "  >> $ARCHIVO
    echo "        BUS USB      "  >> $ARCHIVO
    echo "                     "  >> $ARCHIVO  
    echo "====================="  >> $ARCHIVO
    lsusb  >> $ARCHIVO
    echo "                     "  >> $ARCHIVO
    echo "====================="  >> $ARCHIVO
    echo "                     "  >> $ARCHIVO
    echo "       BUS USB       "  >> $ARCHIVO
    echo "                     "  >> $ARCHIVO  
    echo "====================="  >> $ARCHIVO
    lspci >> $ARCHIVO
    echo ""
}

function usoDiscos {     # Muestra información de los discos Capacidad y ocupación en la maquina que ejecuta el bash
    echo "====================="  >> $ARCHIVO
    echo "                     "  >> $ARCHIVO
    echo "    USO DE DISCOS    "  >> $ARCHIVO
    echo "                     "  >> $ARCHIVO  
    echo "====================="  >> $ARCHIVO
    df -h |grep /dev/ >> $ARCHIVO   
}

function usoRam { # Muestra la información de la memoria RAM de la maquina que ejecuta el bash
    echo "                     "  >> $ARCHIVO
    echo "====================="  >> $ARCHIVO
    echo "                     "  >> $ARCHIVO
    echo "     USO DE RAM      "  >> $ARCHIVO
    echo "                     "  >> $ARCHIVO  
    echo "====================="  >> $ARCHIVO
    free -h | grep +  >> $ARCHIVO
}
function numeroUsuarios { # Muestra el número de usuarios conectados en la maquina que ejecuta el bash
    echo "                     "  >> $ARCHIVO
    echo "====================="  >> $ARCHIVO
    echo "                     "  >> $ARCHIVO
    echo " NUMERO DE USUARIOS  "  >> $ARCHIVO
    echo "                     "  >> $ARCHIVO  
    echo "====================="  >> $ARCHIVO
    who | wc -l >> $ARCHIVO
}
function servicios { # Muestra la información de los servicios arrancados en la maquina que ejecuta el bash
    echo "                      "  >> $ARCHIVO
    echo "======================"  >> $ARCHIVO
    echo "                      "  >> $ARCHIVO
    echo " SERVICIOS ARRANCADOS "  >> $ARCHIVO
    echo "                      "  >> $ARCHIVO  
    echo "======================"  >> $ARCHIVO
    service --status-all | grep + >> $ARCHIVO
}

function puertos { # Muestra los puertos abiertos en la maquina que ejecuta el bash
    echo "                      "  >> $ARCHIVO
    echo "======================"  >> $ARCHIVO
    echo "                      "  >> $ARCHIVO
    echo "   PUERTOS ABIERTOS   "  >> $ARCHIVO
    echo "                      "  >> $ARCHIVO  
    echo "======================"  >> $ARCHIVO
    echo -e "              \e[1;91mBuscando puertos abiertos,Espere un momento  \e[0m"
     nmap -p 0-65535 localhost| grep "open" >> $ARCHIVO
}

function opc1
{
numCpus
dirIP
PuertosUSBPCI
usoDiscos 
usoRam 
numeroUsuarios
servicios
puertos
}

function opc2
{
echo "==================================================="  >> $ARCHIVO
echo "                                                   "  >> $ARCHIVO
echo "                 HORA DEL SISTEMA                  "  >> $ARCHIVO
echo "                                                   "  >> $ARCHIVO  
echo "==================================================="  >> $ARCHIVO
date +%H:%m:%S >> $ARCHIVO
usoDiscos 
usoRam 
}


function fcorreo {
        mail -s "Monitor del servidor " $correo < $ARCHIVO
}

function resol
{
    
  case  $VAR1 in
      "1") 
        touch $ARCHIVO
        opc1
      ;;
      "0") 
       opc2
      ;;
      
 esac
}


# Funcion que muestra el menu
function menu()
{
    fichero
    clear
    echo
    echo
    echo
    echo
    echo ""
    echo "                Selecciona una opcion:"
    echo
    echo "         1) Ejecutar monitorización de servidor"
    echo
    echo "         2) Ejecutar monitorización y envio de correo"
    echo 
    echo "         3) Visualizar fichero de monitorización"
    echo
    echo "         4) Borra ficheros de monitorización "
    echo    
    echo
    echo "         9) Salir"
    echo ""
    
}
 
opc=0
# bucle que no se detiene hasta pulsar el numero nueve
until [ $opc -eq 9 ]
do
    case $opc in
        1)  echo -e "              \e[1;91mEspere un momento  \e[0m"
            echo -e "              \e[1;91mEstamos trabajando  \e[0m"
            resol
            clear
            menu
            ;;

        2)  echo -e "              \e[1;91mEspere un momento  \e[0m"
            echo -e "              \e[1;91mEstamos trabajando  \e[0m"
            resol
            echo -e "              \e[1;91mInserte su dirección de correo: \e[0m" 
            read correo
            
            if [ "$correo" == "" ]; then
                echo -e "\nDirección de correo vacia \n"
                read -n 1 -p "Pulsa una tecla : "
            else
                fcorreo
            fi
               
               clear
            menu
            ;;

        3)  if [ "$ARCHIVO" == "" ]; then
                echo -e "\nNo se puede abrir un fichero vacio utilice la opcion 1 \n"
                read -n 1 -p "Pulsa una tecla : "
            else
              cat -n -s $ARCHIVO | more 
            fi
            clear  
            menu
            ;;

        4)  echo " - Has seleccionado la cuarta opcion del menu"
            rm Monitor_servidor_* 
            ARCHIVO=""
            VAR1=""

            menu
            ;;    
        *)

            # si hemos puslado cualquier valor que no sea correcto
            menu
            ;;
    esac
    read opc
done