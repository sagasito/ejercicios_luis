var express = require('express');  //utilización de express
var mysql      = require('mysql');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var errorhandler =require('errorhandler');
var fs = require('fs');
var app = express();  
var puerto = 3500;  //asigna el puerto 3500.
var config = {};
var connection = null;

// leer datos de conexión a MySql del fichero acceso.json, 
fs.readFile('./acceso.json', (err,data) => {   // funcion arrow  esto es similar a function (err,data)
  config = JSON.parse(data);  
  connection = mysql.createConnection(config);
  connection.connect();
});

// configuración de /public y bodyParser
app.use( bodyParser.json());  
app.use( bodyParser.urlencoded({ extended: true }));  
app.use( express.static('public'));


// ruta BASE
// GET /
app.get('/', function (req, res) {
  res.sendFile( __dirname + '/public/index.html'); 
});


app.get('/tablas' , function (rep, res){  
  connection.query('select table_name as "Nom_Tabla",table_rows as "NumRegistros" from information_schema.tables where table_schema="sakila" and table_rows > 0;', function(error, result){ 
	res.json(result);
});
});

app.get('/actor' , function (rep, res){  
  connection.query('select * from actor;', function(error, result){ 
  res.json(result);
});
});

// borrado de actor:
// desde el cliente se manda un Ajax de type DELETE
app.delete('/actor/:id', (req,res) => {       
  let sql = 'delete from actor where actor_id = ?'
  connection.query(sql, [req.params.id], function (error, results, fields) {
   res.json(results);  
  });
});


app.get('/actor/:id', (req,res) => {
  let sql = 'select * from actor where actor_id = ? limit 1;'
  connection.query(sql, [req.params.id], function (error, results, fields) {     
    // si no se encuentra el registro se devuelve objeto blanco mejor que pantalla blanca
    res.json( results[0] ? results[0] : {} )
  });    
});



app.post('/actor', (req,res) => {     
  let actor = req.body, sql = '' ;
  //console.log(req.body.actor_id, req.body.first_name,req.body.last_name)
  
   if ( parseInt(actor.actor_id) > 0 ){    
      sql = 'update actor set first_name = ? , last_name = ?, last_update = CURRENT_TIMESTAMP where actor_id = ?';
      connection.query(sql, [actor.first_name, actor.last_name, actor.actor_id], function (error, results, fields) { 
       });    
    } else {
      sql = 'insert into actor (first_name,last_name,last_update) values (?, ?, CURRENT_TIMESTAMP)';
      connection.query(sql, [actor.first_name, actor.last_name], function (error, results, fields) {
    });   
  }     
  res.end() 
});



app.get('/address' , function (rep, res){  
  connection.query('select * from address;', function(error, result){ 
  res.json(result);
});
});

app.get('/category' , function (rep, res){  
  connection.query('select * from category;', function(error, result){ 
  res.json(result);
});
});


// borrado de la categoria:

app.delete('/category/:id', (req,res) => {       
  let sql = 'delete from category where category_id = ?'
  connection.query(sql, [req.params.id], function (error, results, fields) {
    res.json(results)
  });
});


app.get('/category/:id', (req,res) => {
  let sql = 'select * from category where category_id = ? limit 1;'
  connection.query(sql, [req.params.id], function (error, results, fields) {     
    // si no se encuentra el registro se devuelve objeto blanco mejor que pantalla blanca
    res.json( results[0] ? results[0] : {} )
  });    
});



app.post('/category', (req,res) => {     
  let category = req.body, sql = '' ;
   if ( parseInt(category.category_id) > 0 ){    
    sql = 'update category set name = ? , last_update = CURRENT_TIMESTAMP where category_id = ?';
    connection.query(sql, [category.name, category.category_id], function (error, results, fields) {      
    });    
  } else {
    sql = 'insert into category (name,last_update) values ( ?, CURRENT_TIMESTAMP)';
    connection.query(sql, [category.name], function (error, results, fields) {
    });   
  }
  res.end()
});


// Gestion de ciudades
app.get('/city' , function (rep, res){  
  connection.query('select * from city;', function(error, result){ 
  res.json(result);
  });
});  

app.delete('/city/:id', (req,res) => {       
  let sql = 'delete from city where city_id = ?'
  connection.query(sql, [req.params.id], function (error, results, fields) {
    res.json(results)
  });
});

app.get('/city/:id', (req,res) => {
  let sql = 'select * from cityy where city_id = ? limit 1;'
   connection.query(sql, [req.params.id], function (error, results, fields) {     
   res.json( results[0] ? results[0] : {} )
  });    
});
// revisar
app.post('/city', (req,res) => {  
  console.log(req.body.city_id, req.body.city,req.body.country_id)
  let city = req.body, sql = '' ;
    if ( parseInt(city.city_id) > 0 ){    
      sql = 'update country set country = ? , last_update = CURRENT_TIMESTAMP where country_id = ?';
      connection.query(sql, [country.country, country.country_id], function (error, results, fields) {   
       });    
    } else {
      sql = 'insert into city (city,country_id,last_update) values ( ?,?, CURRENT_TIMESTAMP)';
      connection.query(sql, [city.city,city.country_id], function (error, results, fields) {
    });   
  }
  res.end()
});

// Gestion de paises
app.get('/country' , function (rep, res){  
  connection.query('select * from country;', function(error, result){ 
  res.json(result);
  });
});

app.delete('/country/:id', (req,res) => {       
  let sql = 'delete from country where country_id = ?'
  connection.query(sql, [req.params.id], function (error, results, fields) {
    res.json(results)
  });
});


app.get('/country/:id', (req,res) => {
  let sql = 'select * from country where country_id = ? limit 1;'
  connection.query(sql, [req.params.id], function (error, results, fields) {     
    // si no se encuentra el registro se devuelve objeto blanco mejor que pantalla blanca
    res.json( results[0] ? results[0] : {} )
  });    
});



app.post('/country', (req,res) => {  
  let country = req.body, sql = '' ;
    if ( parseInt(country.country_id) > 0 ){    
      sql = 'update country set country = ? , last_update = CURRENT_TIMESTAMP where country_id = ?';
      connection.query(sql, [country.country, country.country_id], function (error, results, fields) {   
       });    
    } else {
      sql = 'insert into country (country,last_update) values ( ?, CURRENT_TIMESTAMP)';
      connection.query(sql, [country.country], function (error, results, fields) {
    });   
  }
  res.end()
});



// Gestión de idiomas
app.get('/language' , function (rep, res){  
  connection.query('select * from language;', function(error, result){ 
  res.json(result);
});
});


// borrado del idioma
app.delete('/language/:id', (req,res) => {       
  let sql = 'delete from language where language_id = ?'
  connection.query(sql, [req.params.id], function (error, results, fields) {
    res.json(results)
  });
});


// select del idioma
app.get('/language/:id', (req,res) => {
  let sql = 'select * from language where language_id = ? limit 1;'
   connection.query(sql, [req.params.id], function (error, results, fields) {     
    res.json( results[0] ? results[0] : {} )
  });    
});





// post del idioma
app.post('/language', (req,res) => {     
  let language = req.body, sql = '' ;
   console.log(req.body.language_id,req.body.name )
    if ( parseInt(language.language_id) > 0 ){    
     sql = 'update language set name = ? , last_update = CURRENT_TIMESTAMP where language_id = ?';
     connection.query(sql, [language.name, language.language_id], function (error, results, fields) {      
     });    
  } else {
    sql = 'insert into language (name,last_update) values ( ?, CURRENT_TIMESTAMP)';
    connection.query(sql, [language.name], function (error, results, fields) {
    });   
  }
  res.end()
});



app.get('/customer' , function (rep, res){  
  connection.query('select * from customer;', function(error, result){ 
  res.json(result);
  });
});

app.get('/film' , function (rep, res){  
  connection.query('select * from film;', function(error, result){ 
  res.json(result);
});
});


app.get('/film_actor' , function (rep, res){  
  connection.query('select * from film_actor;', function(error, result){ 
  res.json(result);
});
});


app.get('/film_category' , function (rep, res){  
  connection.query('select * from film_category;', function(error, result){ 
  res.json(result);
});
});


app.get('/film_text' , function (rep, res){  
  connection.query('select * from film_text;', function(error, result){ 
  res.json(result);
});
});

app.get('/inventory' , function (rep, res){  
  connection.query('select * from inventory;', function(error, result){ 
  res.json(result);
});
});



app.get('/payment' , function (rep, res){  
  connection.query('select * from payment;', function(error, result){ 
  res.json(result);
});
});

app.get('/rental' , function (rep, res){  
  connection.query('select * from rental;', function(error, result){ 
  res.json(result);
});
});

app.get('/staff' , function (rep, res){  
  connection.query('select * from staff;', function(error, result){ 
  res.json(result);
});
});

app.get('/store' , function (rep, res){  
  connection.query('select * from store;', function(error, result){ 
  res.json(result);
});
});



app.get('/actor_inf' , function (rep, res){  
  connection.query('select * from actor_info;', function(error, result){ 
	res.json(result);
});
});

app.get('/clientes' , function (rep, res){  
  connection.query('select * from customer_list;', function(error, result){ 
	res.json(result);
});
});

app.get('/peliculas' , function (rep, res){  
  connection.query('select * from film_list;', function(error, result){ 
	res.json(result);
});
});


app.get('/nicer' , function (rep, res){  
  connection.query('select * from nicer_but_slower_film_list;', function(error, result){ 
	res.json(result);
});
});

app.get('/venca' , function (rep, res){  
  connection.query('select * from sales_by_film_category;', function(error, result){ 
	res.json(result);
});
});
app.get('/ventienda' , function (rep, res){  
  connection.query('select * from sales_by_store;', function(error, result){ 
	res.json(result);
});
});
app.get('/staff' , function (rep, res){  
  connection.query('select * from staff_list;', function(error, result){ 
	res.json(result);
});
});

// Vistas de Sakila
app.get('/tabla/:nomtabla' , function (rep, res){  
  req.params.nomtabla
  connection.query('select * from '+ req.params.nomtabla +';', function(error, result){ 
  res.json(result);
});
});






app.listen(puerto, function(){
  console.log('Servidor escuchando en el puerto  ' + puerto);
});




