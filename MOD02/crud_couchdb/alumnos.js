//const database = 'http://192.168.110.243:15984/alumnos/'
const database = 'http://127.0.0.1:15984/alumnos/'
var registro =""
var cargaimagen = new FileReader()	
var contenido = ''  
var stringAttach = ''
var list =''
var adjunto ={}
var origen1 =''
var idimg =''

// llamada a ajax a una BBDD por defecto
var defaultAjax = {
	 type: "get",
	 url: database +"_all_docs?include_docs=true",
	 data: null,
	 dataType:'json',
	 contentType:"application/json; charset=utf-8"
    }

function getAlumnos(origen){
	defaultAjax.type = "get"
	//console.log(origen)
	origen1=origen1
	// limpia los datos existentes en el formulario
	anular()
	$('#fcargaimagen').hide();
	// limpia la imagen del formulario
	document.getElementById("vfotografia").src=""

	if (origen == "LA") {
    $('#listalumnos').show();
    $('#listadni').hide();
    $('#listadonombre').hide();
    $('#listadoapellidos').hide();
   $('#listados').show();
 	{
	defaultAjax.url = database + "_all_docs?include_docs=true"
     $.ajax(defaultAjax).done(function(data) {
		//console.log(data)
		 $('#selector').html('')
		$.each (data.rows, function (indice,registro) {
			let html =`<option value ="${registro.id}">
			${registro.doc.dni}:  ${registro.doc.apellidos} ${registro.doc.nombre} 
			${registro.doc.email} </option>`
             $('#selector').append(html)
 		});
	});
   }
}
   if (origen == "D" || list =="D") {
  	list ="D"
   	$('#listalumnos').hide();
    $('#listadni').show();
    $('#listadonombre').hide();
    $('#listadoapellidos').hide();
   
	defaultAjax.url = database + "_design/app/_view/dni"
	//console.log(defaultAjax.url)
     $.ajax(defaultAjax).done(function(data) {
		 $('#selector').html('')
		$.each (data.rows, function (indice,registro) {
           let html =`<option value ="${registro.id}">
			${registro.key.dni}:  
			 ${registro.key.apellidos} ${registro.key.nombre} 
			 </option>`
             $('#selector').append(html)
  		});
	});
   }

if (origen == "N" || list =="N") {
  	list ="N"
	$('#listalumnos').hide();
    $('#listadni').hide();
    $('#listadonombre').show();
    $('#listadoapellidos').hide();
    //$('#listados').show();
	defaultAjax.url = database + "_design/app/_view/nombre"
	  $.ajax(defaultAjax).done(function(data) {
		 $('#selector').html('')
		$.each (data.rows, function (indice,registro) {
			let html =`<option value ="${registro.id}">
			${registro.key.nombre}:  
			 ${registro.key.apellidos}  
			 </option>`
             $('#selector').append(html)
   		});
	});
   }

if (origen == "A" || list =="A") {
  	list ="A"
	$('#listalumnos').hide();
    $('#listadni').hide();
    $('#listadonombre').hide();
    $('#listadoapellidos').show();
    $('#listados').show();
	defaultAjax.url = database + "_design/app/_view/apellidos"
	//console.log(defaultAjax.url)
     $.ajax(defaultAjax).done(function(data) {
		 $('#selector').html('')
		$.each (data.rows, function (indice,registro) {
			let html =`<option value ="${registro.id}">
			${registro.key.apellidos}:  
			 ${registro.key.nombre}  
			 </option>`
             $('#selector').append(html)
         });
	});
   }
}

function cargaFalumnos(){
	document.getElementById("vfotografia").src=""
	$('#cabnalumnos').show();
	$('#falumnos').show();
	$('#imagen').show();
	$('#fcargaimagen').hide();
	$('#botonCrear').show();
	$('#botonModificar').hide();
	$('#listados').hide();
	$('#menu').hide();	
	$('#botoneslistados').hide();
  }


function gestionAlumnos(){
	$('#botoneslistados').hide();
	$('#botonCrear').hide();
	$('#botonModificar').hide();
 	$('#menu').hide();
	$('#menuAlumnos').show();
	$('#listalumnos').show();
    $('#listadni').hide();
    $('#listadonombre').hide();
    $('#listadoapellidos').hide();
    $('#listados').hide();
	$('#fcargaimagen').hide();
	getAlumnos('LA');
}

function about(){
	prompt("mensaje personalizado", "valor por defecto");
}
	
function volver(){
	document.getElementById("contact_form").reset();
	 $('#menu').show();	
	 $('#falumnos').hide();
	 $('#menuAlumnos').hide();
	 $('#listados').hide();
	 $('#botoneslistados').hide();
	 $('#botonCrear').hide();
     $('#botonModificar').hide();
     $('#cabnalumnos').hide();
  }

 // Limpia los datos del formulario
function anular() {
	document.getElementById("contact_form").reset();
}


function cambiaCombo(){
	registro = $('#selector option:selected').val()
    var idimg =''
   	defaultAjax.url =database + $('#selector option:selected').val()
		$.ajax(defaultAjax).done(function(data){
			$('#id').val(data._id)  // para realizar una modificacion o borrado hay que recoger el _id y _rev
			$('#rev').val(data._rev)
			$('#dni').val(data.dni)
			$('#apellidos').val(data.apellidos)
			$('#nombre').val(data.nombre)
			$('#telf').val(data.telefono)
			$('#email').val(data.email)
            $('#nota').val(data.nota)
            $('#fcargaimagen').hide();
            adjunto = data._attachments
               for (nombre in data._attachments) {
            	  if ($('#fotografia').val(nombre).length = 1 ){
            	      $('#fcargaimagen').show();
            	   	  document.getElementById("vfotografia").src=(database + $('#id').val()+ '/' + $('#fotografia').val())
             	      }
            	} 
            
    	}) 
}

function modificar(){
//1.recoge los inputs del formulario
if(validar_dni_nif_nie($("#dni").val())
          && ( $('#apellidos').val().length > 1  )
          && (  $('#nombre').val().length > 1 )
          && (  $('#email').val().length > 1 )
			){

let doc = {
	_id:$('#id').val(),
	_rev:$('#rev').val(),
	dni:$('#dni').val().toUpperCase(),
	apellidos:$('#apellidos').val().toUpperCase(),
	nombre: $('#nombre').val().toUpperCase(),
	telefono: $('#telf').val(),
	email:$('#email').val(),
	nota:$('#nota').val(),
    _attachments:adjunto
    }
    //2 enviar POST a CouchDb
    defaultAjax.type ='post'
    defaultAjax.url = database 
    defaultAjax.data =JSON.stringify(doc)
    $.ajax(defaultAjax).done(function(data){
    	alert("Los datos del alumno  " + doc.nombre +" " +doc.apellidos +" ha sido modificado correctamente");
    })
     getAlumnos('') // carga nuevamente la lista de alumnos ordenados segun criterio
 }
 else {alert(" NIF/NIE incorrectos o faltan datos obligatorios ");      }
}


// Función borrar documento
function borrar(){ 
	let doc = {
	_id:$('#id').val(),
	_rev:$('#rev').val(),
	apellidos:$('#apellidos').val(),
	nombre: $('#nombre').val()
   }
    defaultAjax.type ='delete'
    defaultAjax.url = database  + doc._id + '?rev=' + doc._rev
		alumno = null	
		$.ajax(defaultAjax).done(function(data){
		alert("Los datos del alumno " + doc.nombre +" " + doc.apellidos +" ha sido borrados correctame");
    })
	    document.getElementById("contact_form").reset();
	    $('#selector').html('')
	    document.getElementById("vfotografia").src=""
        getAlumnos('')
}


function validar_dni_nif_nie(value){
     var validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
	 var nifRexp = /^[0-9]{1,8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
	 var nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
	 var str = value.toString().toUpperCase();
     	  if (!nifRexp.test(str) && !nieRexp.test(str)) return false;
 			  var nie = str
				  .replace(/^[X]/, '0')
				  .replace(/^[Y]/, '1')
				  .replace(/^[Z]/, '2');
				  var letter = str.substr(-1);
				  var charIndex = parseInt(nie.substr(0, 8)) % 23;
 				  if (validChars.charAt(charIndex) === letter) return true;
 				  return false;
}

function selFichero(archivo){	
	cargaimagen.onloadend = function () {		
		contenido = cargaimagen.result.split('base64,')[1]		
		stringAttach = `{ "${archivo.name}": {			
			  "content_type": "${archivo.type}",	
			  "data": "${contenido}"
			}
		}
		`
		stringObjeto = `
			{
				"tipoDoc": "fotografia",
				"_attachments": {
					"${archivo.name}": {			
					  "content_type": "${archivo.type}",	
					  "data": "${contenido}"
					}
				}
			}
		`
	  }  

   	cargaimagen.readAsDataURL(archivo)	
	//console.log(archivo)
	   // console.log(stringObjeto)	

}


function crear(){
	if (contenido ==""  ){
		if(validar_dni_nif_nie($("#dni").val())

          && ( $('#apellidos').val().length > 1  )
          && (  $('#nombre').val().length > 1 )
          && (  $('#email').val().length > 1 )
			){
	let doc = {
		dni:$('#dni').val().toUpperCase(),
		apellidos:$('#apellidos').val().toUpperCase(),
		nombre: $('#nombre').val().toUpperCase(),
		telefono: $('#telf').val(),
		email:$('#email').val()
        }
        //2 enviar POST a CouchDb
	    defaultAjax.type ='post'
	    defaultAjax.url = database 
	    defaultAjax.data =JSON.stringify(doc)
	    document.getElementById("contact_form").reset();
	    $.ajax(defaultAjax).done(function(doc){
	    })
	   alert("Alumno creado correctamente" );
	    document.getElementById("contact_form").reset();
	    // Vacia los valores y los deja en blanco despues de grabar el dato
	    
	   // else{
	//		 alert("Documento no es válido");
		//	}
	}
  else{
	  alert(" NIF/NIE incorrectos o faltan datos obligatorios");
      document.getElementById("contact_form").reset();
	   }
  }
  else {
   	if(validar_dni_nif_nie($("#dni").val())
          && ( $('#apellidos').val().length > 1  )
          && (  $('#nombre').val().length > 1 )
          && (  $('#email').val().length > 1 )
			){
	let doc = {
		dni:$('#dni').val().toUpperCase(),
		apellidos:$('#apellidos').val().toUpperCase(),
		nombre: $('#nombre').val().toUpperCase(),
		telefono: $('#telf').val(),
		email:$('#email').val(),
		_attachments: JSON.parse(stringAttach)
        }
  //2 enviar POST a CouchDb
	    defaultAjax.type ='post'
	    defaultAjax.url = database 
	    defaultAjax.data =JSON.stringify(doc)
	    document.getElementById("contact_form").reset();
	    $.ajax(defaultAjax).done(function(doc){
	    })
	   alert("Alumno creado correctamente" );
	    document.getElementById("contact_form").reset();
	    // Vacia los valores y los deja en blanco despues de grabar el dato
	    
	   // else{
	//		 alert("Documento no es válido");
		//	}
	}
  else{
	  alert(" NIF/NIE incorrectos o faltan datos obligatorios ");
      document.getElementById("contact_form").reset();
	   }
	//}  else {
	}
  };


$(document).ready(function(){
	$('#menuAlumnos').hide();
	$('#falumnos').hide();
	$('#listados').hide();
	$('#botoneslistados').hide();
	$('#botonCrear').hide();
	$('#botonModificar').hide();
	$('#cabnalumnos').hide();
});


function validar_dni_nif_nie(value){
 
				  var validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
				  var nifRexp = /^[0-9]{1,8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
				  var nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
				  var str = value.toString().toUpperCase();
 
				  if (!nifRexp.test(str) && !nieRexp.test(str)) return false;
 
				  var nie = str
					  .replace(/^[X]/, '0')
					  .replace(/^[Y]/, '1')
					  .replace(/^[Z]/, '2');
 
				  var letter = str.substr(-1);
				  var charIndex = parseInt(nie.substr(0, 8)) % 23;
 
				  if (validChars.charAt(charIndex) === letter) return true;
 
				  return false;
			}
 
			$(document).ready(function(){
            
            $("#Modificar1").click(function(evento){
						//$("#btn_validar").click(function(evento){
								if(validar_dni_nif_nie($("#dni").val())){

									let doc = {
	//dni:$('#dni').validaNif(this),
	apellidos:$('#apellidos').val(),
	nombre: $('#nombre').val(),
	telefono: $('#telf').val(),
	email:$('#email').val()
    }
    //2 enviar POST a CouchDb
    defaultAjax.type ='post'
    defaultAjax.url = database 
    defaultAjax.data =JSON.stringify(doc)
    $.ajax(defaultAjax).done(function(data){
    })
									//alert("Es válido");
								}else{
									alert("Documento no es válido");
								}
                        });
						
            })	

function listadoAlunmos(valor) {
	//console.log(valor)
	//$('#listados1').show();
	$('#listados').show();
    $('#listadni').hide();
    $('#listadonombre').hide();
    $('#listadoapellidos').hide();
	$('#imagen').hide();
	$('#falumnos').show();
	//$('#cargaimagen').show();
	$('#botonModificar').show();
	$('#cabnalumnos').hide();
	getAlumnos(valor);


}